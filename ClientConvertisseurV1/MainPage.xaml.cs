﻿using ClientConvertisseurV1.Models;
using ClientConvertisseurV1.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientConvertisseurV1
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ActionGetData();
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }
        private async void ActionGetData() {
            var result = await WSService.Instance.GetAllDevisesAsync();
            this.cbxDevise.DataContext = new List<Devise>(result); 
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string montant = this.txtMontant.Text;
            Devise devise = (Devise)this.cbxDevise.SelectedItem;
            double conversion = Convertir(montant, devise);
            txtResultat.Text = conversion.ToString();
        }

        private double Convertir(string montant, Devise devise)
        {
            double resultat = 0;
            if(!double.TryParse(montant, out resultat))
            {
                MessageErreur();
            }

            return resultat*devise.Taux;
        }

        private async void MessageErreur()
        {
            Windows.UI.Popups.MessageDialog message = new MessageDialog("Montant non valide !");
            await message.ShowAsync();
        }
    }
}
