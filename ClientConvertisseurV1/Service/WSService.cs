﻿using ClientConvertisseurV1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;

namespace ClientConvertisseurV1.Service
{
    public class WSService
    {
        #region VARIABLES
        private static WSService instance = null;
        private List<Devise> devises;
        private HttpClient client;
        #endregion

        #region CONSTRUCTEUR
        private WSService()
        {
            client = new HttpClient();
        }
        #endregion

        #region INSTANCE
        public static WSService Instance { get {
                if (instance == null)
                {
                    instance = new WSService();
                }
                return instance;
            }
        }
        #endregion

    #region METHODES
        public async Task<List<Devise>> GetAllDevisesAsync()
        {
            HttpResponseMessage response = await client.GetAsync("http://localhost:5000/api/devise");
            if (response.IsSuccessStatusCode)
            {
                devises = await response.Content.ReadAsAsync<List<Devise>>();
            }
        return devises;
        }
    #endregion
    }
}
